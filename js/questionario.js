$(document).ready(function(){
    /*Parte approfondimento*/

    var fa;

    $(".tasto-approfondimento").click(function(){
        var id = $(this).attr("id");
        fa = $("#finestra-approfondimento-domanda" + id.substring(29,id.length))[0];
        fa.style.display = "block";
    })

    $(".chiudi").click(function(){
        fa.style.display = "none";
    })

    window.onclick = function(event){
        if (event.target == fa) {
            fa.style.display = "none";
        }
    }

    /*Parte beneficio*/

    $("div.beneficio").hide();
    $("div.consiglio").hide();
    $("fieldset.sottodomanda").hide();

    var prev = [];
    var numeroDomanda = [];
    var beneficio;
    var b = 0;
    $("input[type='radio']:not(.radio-beneficio)").click(function(){
        if(prev.length > 0 && numeroDomanda.includes($(this).attr("name").match(/(\d+)/)[0])){
            var index = numeroDomanda.indexOf($(this).attr("name").match(/(\d+)/)[0]);
            prev[index][0].style.display = "none";
            numeroDomanda.splice(index, 1);
            prev.splice(index, 1);
            if(b == 1){
                b = 0;
                beneficio.prop("checked", false)
            }
        }
        x = $(this).next().next().next();
        if(x.is(".beneficio") || x.is(".consiglio") || x.is(".sottodomanda")){
           x.show();
           prev.push(x);
           numeroDomanda.push($(this).attr("name").match(/(\d+)/)[0]);
        }
    })

    $(".radio-beneficio").click(function(){
        b = 1;
        beneficio = $(this);
    })

})