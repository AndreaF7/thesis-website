$(document).ready(function(){
    if($("#cambiadati").length > 0){
        $("#cambiadati").on("submit",function(){
            var vpass = $("input[name=vecchia-password").val();
            var pass = $("input[name=password]").val();
            var cpass= $("input[name=conferma-password]").val();
            if(pass != "" && cpass != "" && vpass != ""){
                pass = CryptoJS.SHA256(pass);
                vpass = CryptoJS.SHA256(vpass);
                cpass = CryptoJS.SHA256(cpass);
                $("input[name=vecchia-password]").val(vpass);
                $("input[name=conferma-password]").val(cpass);
                $("input[name=password]").val(pass);
                $("select[name=quartiere]").val() = $("select[name=quartiere]").val();
            }
        })
    }

    $("#form-registrazione").on("submit",function(){
        var pass = $("input[name=password]").val();
        var cpass= $("input[name=conferma-password]").val();
        if(pass != "" && cpass != ""){
            pass = CryptoJS.SHA256(pass);
            cpass = CryptoJS.SHA256(cpass);
            $("input[name=conferma-password]").val(cpass);
            $("input[name=password]").val(pass);
        }
    })

    $("#form-login").on("submit",function(){
        var pass = $("input[name=password]").val();
        if(pass != ""){
            pass = CryptoJS.SHA256(pass);
            $("input[name=password]").val(pass);
        }
    })

    $("input[name=cesena]").on("click",function(){
        $("select[name=quartiere]").attr("disabled", !$("select[name=quartiere]").attr("disabled"));
    })

})