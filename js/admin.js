$(document).ready(function(){
    var nb = new Array(9).fill(0);
    var nc = new Array(9).fill(0);
    var sd = new Array(9).fill(0);
    var rs = 2;
    $("input[name*='aggiungi-beneficio").click(function(){
        var r = $(this).attr("name")[$(this).attr("name").length - 1];
        nb[r] ++;
        var pa = $(this).parent();
        pa.append("<div id=r" + r + "-b" + nb[r] + "><label for=r" + r + "-b" + nb[r] + ">Beneficio:</label><br><textarea id=r" + r + "-b" + nb[r] + " name=r" + r + "-b" + nb[r] + " placeholder=Opzionale></textarea></div>");
    })
    $(document).on("click","input[name*=rimuovi-beneficio]", function(){
        var r = $(this).attr("name")[$(this).attr("name").length - 1];
        $("div#r" + r + "-b" + nb[r]).remove();
        if(nb[r] > 0){
            nb[r]--;
        }
    })

    $("input[name*='aggiungi-consiglio").click(function(){
        var r = $(this).attr("name")[$(this).attr("name").length - 1];
        nc[r] ++;
        var pa = $(this).parent();
        pa.append("<div id=r" + r + "-c" + nc[r] + "><label for=r" + r + "-c" + nc[r] + ">Consiglio:</label><br><textarea id=r" + r + "-c" + nc[r] + " name=r" + r + "-c" + nc[r] + " placeholder=Opzionale></textarea></div>");
    })
    $(document).on("click","input[name*=rimuovi-consiglio]", function(){
        var r = $(this).attr("name")[$(this).attr("name").length - 1];
        $("div#r" + r + "-c" + nc[r]).remove();
        if(nc[r] > 0){
            nc[r]--;
        }
    })

    $("input[name*='aggiungi-sottodomanda").click(function(){
        var r = $(this).attr("name")[$(this).attr("name").length - 1];
        if(sd[r] < 1){
            sd[r] ++;
            var pa = $(this).parent();
            pa.append("<div id=r" + r + "-sd>" +
            "<label for=r" + r + "-sd>Sottodomanda:</label><br>"+
            "<textarea id=r" + r + "-sd name=r" + r + "-sd placeholder=Opzionale></textarea><br>"+
            "<label for=sotto-domanda-r"+r+"1>Risposta 1:</label>"+
            "<input type=text id=sotto-domanda-r"+r+"1 name=sotto-domanda-r"+r+"-1 required>"+
            "<label for=sotto-domanda-r"+r+"2>Risposta 2:</label>"+
            "<input type=text id=sotto-domanda-r"+r+"2 name=sotto-domanda-r"+r+"-2 required></div>");
        }
    })
    $(document).on("click","input[name*=rimuovi-sottodomanda]", function(){
        var r = $(this).attr("name")[$(this).attr("name").length - 1];
        if(sd[r] == 1){
            $("div#r" + r + "-sd").remove();
            sd[r] --;
        }
    })

    $("input[name=agg-risp]").on("click", function(){
        rs++;
        $("#risposte").append('<div>'+
        '<label for="aggiungi-risposta'+rs+'">Risposta '+rs+':</label>'+
        '<input type="text" id="aggiungi-risposta'+rs+'" name="aggiungi-risposta'+rs+'" placeholder=Opzionale>'+
        '<label for="punteggio-r'+rs+'">Punteggio: </label>'+
        '<input type="number" id="punteggio-r'+rs+'" name="punteggio-r'+rs+'" min="0" max="10">'+
        '<br>'+
        '<label for="aggiungi-beneficio-r'+rs+'">Aggiungi beneficio</label>'+
        '<input type="button" name="aggiungi-beneficio-r'+rs+'" value="+">'+
        '<input type="button" name="rimuovi-beneficio-r'+rs+'" value=->'+
        '<label for="aggiungi-consiglio-r'+rs+'">Aggiungi consiglio</label>'+
        '<input type="button" name="aggiungi-consiglio-r'+rs+'" value="+">'+
        '<input type="button" name="rimuovi-consiglio-r'+rs+'" value=->'+
        '<label for="aggiungi-sottodomanda-r'+rs+'">Aggiungi sottodomanda</label>'+
        '<input type="button" name="aggiungi-sottodomanda-r'+rs+'" value="+">'+
        '<input type="button" name="rimuovi-sottodomanda-r'+rs+'" value=->'+
        '</div>'+
        '<br>');

        $("input[name=aggiungi-beneficio-r"+rs+"").click(function(){
            var r = $(this).attr("name")[$(this).attr("name").length - 1];
            nb[r] ++;
            var pa = $(this).parent();
            pa.append("<div id=r" + r + "-b" + nb[r] + "><label for=r" + r + "-b" + nb[r] + ">Beneficio:</label><br><textarea id=r" + r + "-b" + nb[r] + " name=r" + r + "-b" + nb[r] + " placeholder=Opzionale></textarea></div>");
        })
        $(document).on("click","input[name=rimuovi-beneficio-r"+rs+"]", function(){
            var r = $(this).attr("name")[$(this).attr("name").length - 1];
            $("div#r" + r + "-b" + nb[r]).remove();
            if(nb[r] > 0){
                nb[r]--;
            }
        })
    
        $("input[name=aggiungi-consiglio-r"+rs+"").click(function(){
            var r = $(this).attr("name")[$(this).attr("name").length - 1];
            nc[r] ++;
            var pa = $(this).parent();
            pa.append("<div id=r" + r + "-c" + nc[r] + "><label for=r" + r + "-c" + nc[r] + ">Consiglio:</label><br><textarea id=r" + r + "-c" + nc[r] + " name=r" + r + "-c" + nc[r] + " placeholder=Opzionale></textarea></div>");
        })
        $(document).on("click","input[name=rimuovi-consiglio-r"+rs+"]", function(){
            var r = $(this).attr("name")[$(this).attr("name").length - 1];
            $("div#r" + r + "-c" + nc[r]).remove();
            if(nc[r] > 0){
                nc[r]--;
            }
        })
    
        $("input[name=aggiungi-sottodomanda-r"+rs+"").click(function(){
            var r = $(this).attr("name")[$(this).attr("name").length - 1];
            if(sd[r] < 1){
                sd[r] ++;
                var pa = $(this).parent();
                pa.append("<div id=r" + r + "-sd>" +
                "<label for=r" + r + "-sd>Sottodomanda:</label><br>"+
                "<textarea id=r" + r + "-sd name=r" + r + "-sd placeholder=Opzionale></textarea><br>"+
                "<label for=sotto-domanda-r"+r+"1>Risposta 1:</label>"+
                "<input type=text id=sotto-domanda-r"+r+"1 name=sotto-domanda-r"+r+"-1 required>"+
                "<label for=sotto-domanda-r"+r+"2>Risposta 2:</label>"+
                "<input type=text id=sotto-domanda-r"+r+"2 name=sotto-domanda-r"+r+"-2 required></div>");
            }
        })
        $(document).on("click","input[name=rimuovi-sottodomanda-r"+rs+"]", function(){
            var r = $(this).attr("name")[$(this).attr("name").length - 1];
            if(sd[r] == 1){
                $("div#r" + r + "-sd").remove();
                sd[r] --;
            }
        })
    })

    $("input[name=rim-risp]").on("click", function(){
        if(rs > 2) {
            $("#risposte").children().last().remove();
            $("#risposte").children().last().remove();
            console.log($("#risposte").children().last());
            rs--;
        }
    })
})