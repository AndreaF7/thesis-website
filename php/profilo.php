<?php
require_once 'bootstrap.php';
if(!isset($_SESSION["username"])){
    header("Location: index.php");
}else{
    $tp["titolo"] = "Profilo";
    $tp["nome"] = "template/profilo.php";
    $tp["header"] = 3;
    if(isset($_POST) && count($_POST) > 0){
        $tp["notifica"] = "";
        $vp = $_POST["vecchia-password"];
        if(!empty($vp)){
            $user = $dbh->checkLogin($_SESSION["username"], $vp);
            if(count($user) > 0){
                $np = $_POST["password"];
                $cp = $_POST["conferma-password"];
                if($np == $cp && !empty($np) && !empty($cp)){
                    $dbh->updatePassword($_SESSION["username"],$np);
                    $tp["notifica"] = "Password correttamente modificata<br>";
                    $tp["erroreModifica"] = 0;
                }else{
                    $tp["notifica"] = "La password nuova e la conferma non coincidono";
                    $tp["erroreModifica"] = 1;
                }
            }else{
                $tp["notifica"] = "La password vecchia non è corretta";
                $tp["erroreModifica"] = 1;
            }
        }
        if(isset($_POST["cesena"]) && $_POST["cesena"] == "si"){
            $quartiere = substr($_POST["quartiere"],1);
        } else{
            $quartiere = null;
        }
        if($quartiere != $_SESSION["quartiere"]){
            $dbh->updateQuarter($_SESSION["username"],$quartiere);
            $_SESSION["quartiere"] = $quartiere;
            $tp["notifica"] .= "Quartiere correttamente modificato";
            $tp["erroreModifica"] = 0;
        }
    }
}
require 'template/base.php';
?>