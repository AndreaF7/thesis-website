<?php
require_once 'bootstrap.php';
if(!isset($_SESSION["username"]) || empty($dbh->checkAdmin($_SESSION["username"]))){
    header("Location: index.php");
}else{
    if(isset($_POST["vidq"])){
        $_SESSION["Idq"] = $_POST["vidq"];
        if(isset($_POST["cancella"])){
            $dbh->deleteQuest($_SESSION["Idq"]);
            header("Location: admin.php");
        }
    }
    $tp["titolo"] = "Admin";
    $tp["nome"] = "template/admin-visualizza-form.php";
    $tp["header"] = 3;
}
require 'template/base.php';
?>