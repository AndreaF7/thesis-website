<?php

class DataBaseHelper{
    private $db;

    public function __construct($servername, $username, $password, $dbname){
        $this->db = new mysqli($servername, $username, $password, $dbname);
        if($this->db->connect_error){
            die("Connection failed: " . $this->db->connect_error);
        }
    }

    //CONTROLLA SE USERNAME E PASSWORD SONO CORRETTI E RESTITUISCE LE INFO SE PRESENTE
    public function checkLogin($username, $password){
        $query = "SELECT username, id_quartiere FROM utente WHERE username = ? and `password` = ?";
        $stmt =  $this->db->prepare($query);
        $stmt->bind_param("ss",$username,$password);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    //CONTROLLA SE ESISTE GIA' UN UTENTE CON QUEL USERNAME
    public function checkUser($username){
        $query = "SELECT Username,Email FROM utente WHERE Username = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$username);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    //REGISTRA UN UTENTE
    public function registerUser($username,$password,$email,$quartiere){
        $query = "INSERT INTO `utente` (`Username`, `Email`, `Password`, `Admin`, `Id_Quartiere`) VALUES (?, ?, ?, NULL, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("sssi",$username,$email,$password,$quartiere);
        $stmt->execute();
    }

    //RESTITUISCE TUTTI I QUARTIERI REGISTRATI
    public function getPlaces(){
        $query = "SELECT Id, Nome FROM quartiere";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    /*

    QUESTIONARIO

    */

    //DATO UN NUMERO DI QUESTIONARIO RESTITUISCE LE DOMANDE AD ESSO ASSOCIATE
    public function getQuestions($questionario){
        $query = "SELECT * FROM Domanda WHERE Id_Questionario = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i",$questionario);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    //DATO UN QUESTIONARIO RESTITUISCE LE DOMANDE PRINCIPALI
    public function getMainQuestions($idq){
        $query = "SELECT * FROM Domanda WHERE Id_Questionario = ? AND Id_Sottodomanda IS NULL";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i",$idq);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    //DATA UNA DOMANDA RESTITUISCE L'APPROFONDIMENTO
    public function getQuestionInfo($domanda,$idq){
        $query = "SELECT Link, A.Testo FROM Domanda D, Approfondimento A WHERE D.Id_Approfondimento = A.Id AND D.Num = ?
        AND D.Id_Questionario = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ii",$domanda,$idq);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    //DATA UNA DOMANDA RESTITUISCE LE RISPOSTE POSSIBILI
    public function getQuestionAnswers($domanda,$idq){
        $query = "SELECT DISTINCT(R.Id), R.* FROM Risposta R, Domanda D WHERE
        D.Num = R.Num AND D.Num = ? AND R.Id_Questionario = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ii",$domanda,$idq);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    //DATA UNA RISPOSTA RESTITUISCE I BENEFICI ASSOCIATI
    public function getAnswerBenefits($idr){
        $query = "SELECT B.Id, B.Testo FROM Beneficio B, Beneficio_per_risposta R WHERE
        R.Id_Beneficio = B.Id AND R.Id_Risposta = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i",$idr);
        $stmt->execute();
        $result = $stmt->get_result();
        
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    //DATA UNA RISPOSTA RESTITUISCE I CONSIGLI ASSOCIATI
    public function getAnswerAdvices($idr){
        $query = "SELECT C.Testo FROM Consiglio C, Visualizza R WHERE
        R.Id_Consiglio = C.Id AND R.Id_Risposta = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i",$idr);
        $stmt->execute();
        $result = $stmt->get_result();
        
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    //DATA UNA RISPOSTA RESTITUISCE LA SOTTODOMANDA ASSOCIATA
    public function getSubQuestion($idr){
        $query = "SELECT D.Num, D.Testo FROM Domanda D WHERE D.Id_Sottodomanda = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i",$idr);
        $stmt->execute();
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    }

    //RESTITUISCE LA PERCENTUALE DI CONFERME PER UN DATO BENEFICIO
    public function getBenefitPercentage($idb){
        $query = "SELECT COUNT(*) AS c FROM conferma WHERE Id_Beneficio = ? AND Conferma = 1";
        //Conferme
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i",$idb);
        $stmt->execute();
        $c = $stmt->get_result()->fetch_all(MYSQLI_ASSOC)[0]["c"];
        $query2 = "SELECT COUNT(*) AS n FROM conferma WHERE Id_Beneficio = ?";
        //Benefici totali
        $stmt2 = $this->db->prepare($query2);
        $stmt2->bind_param("i",$idb);
        $stmt2->execute();
        $n = $stmt2->get_result()->fetch_all(MYSQLI_ASSOC)[0]["n"];
        return $n > 0 ? ($c / $n) * 100 : 0;
    }

    /*

    INVIO QUESTIONARIO

    */

    //DATO UN ID COMPILAZIONE E ID RISPOSTA LE COLLEGA
    public function sendAnswer($idc,$idr){
        $query = "INSERT INTO `riferisce` (`Id_Compilazione`, `Id_Risposta`) VALUES(?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ii",$idc,$idr);
        $stmt->execute();
    }

    //DATO UNO USER E UN QUESTIONARIO REGISTRA LA COMPILAZIONE
    public function compileForm($username,$idq){
        $query = "INSERT INTO compilazione (Data_Compilazione, Id, C_Q_Id, Username)
        VALUES(CURRENT_DATE(), NULL, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("is",$idq,$username);
        $stmt->execute();
        $query2 = "SELECT LAST_INSERT_ID()";
        $stmt2 = $this->db->prepare($query2);
        $stmt2->execute();
        return $stmt2->get_result()->fetch_row();
    }

    //DATO IL NUMERO RISPOSTA E BENEFICIO AGGIUNGE LA CONFERMA DELL'UTENTE
    public function sendBenefitConfirm($user,$idr,$idb){
        $query = "INSERT INTO `conferma` (`Username`, `Id_Risposta`, `Id_Beneficio`, Conferma)
        VALUES(?, ?, ?, 1)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("sii",$user,$idr,$idb);
        $stmt->execute();
    }

    //DATO IL NUMERO RISPOSTA E BENEFICIO RESTITUISCE LA CONFERMA DELL'UTENTE SE PRESENTE
    public function getBenefitConfirm($user,$idr,$idb){
        $query = "SELECT * FROM conferma WHERE Username = ? AND Id_Risposta = ? AND Id_Beneficio = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("sii",$user,$idr,$idb);
        $stmt->execute();
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC) ;
    }

    //DATO IL NUMERO RISPOSTA E BENEFICIO RIMUOVE LA CONFERMA DELL'UTENTE
    public function updateBenefitConfirm($user,$idr,$idb,$conferma){
        $query = "UPDATE `conferma` SET Conferma = ? WHERE Username = ? AND Id_Risposta = ? AND Id_Beneficio = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("isii",$conferma,$user,$idr,$idb);
        $stmt->execute();
    }

    //DATA UNA DOMANDA RESTITUISCE SE E' SOTTODOMANDA O MENO
    public function checkSubQuestion($num,$idq){
        $query = "SELECT COUNT(*) FROM domanda WHERE Id_Questionario = ? AND Num = ? AND Id_Sottodomanda IS NOT NULL";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ii",$idq,$num);
        $stmt->execute();
        return $stmt->get_result()->fetch_row()[0] > 0;
    }

    /*

    MODIFICA PROFILO

    */

    //MODIFICA IL QUARTIERE DI RESIDENZA DI UN UTENTE
    public function updateQuarter($user,$quartiere = null){
        $query = "UPDATE utente SET Id_Quartiere = ? WHERE Username = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("is",$quartiere,$user);
        $stmt->execute();
        $result = $stmt->get_result();
    }

    //MODIFICA LA PASSWORD DELL'UTENTE
    public function updatePassword($user,$password){
        $query = "UPDATE utente SET `Password` = ? WHERE Username = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ss",$password,$user);
        $stmt->execute();
        $result = $stmt->get_result();
    }

    /*

    ADMIN

    */

    //CONTROLLA SE L'UTENTE E' ADMIN
    public function checkAdmin($user){
        $query = "SELECT * FROM utente WHERE Username = ? AND `Admin` = 1";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("s",$user);
        $stmt->execute();
        $result = $stmt->get_result();
        
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    //RESTITUISCE IL NUMERO DELL'ULTIMA DOMANDA
    public function getLastQuestionNumber($idq){
        $query = "SELECT Max(Num) FROM domanda WHERE Id_Questionario = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i",$idq);
        $stmt->execute();
        $result = $stmt->get_result();
        
        return $result->fetch_row();
    }

    //INSERISCE UN APPROFONDIMENTO
    public function addInfo($testo, $link = null){
        if($link != null){
            $query = "INSERT INTO approfondimento VALUES(NULL,?,?)";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param("ss",$link,$testo);
        }else{
            $query = "INSERT INTO approfondimento VALUES(NULL,NULL,?)";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param("s",$testo);
        }
        $stmt->execute();

        $query2 = "SELECT LAST_INSERT_ID()";
        $stmt2 = $this->db->prepare($query2);
        $stmt2->execute();
        return $stmt2->get_result()->fetch_row();
    }

    //INSERISCE UNA DOMANDA
    public function addQuestion($idq,$domanda,$na=null,$avanzata=null,$sottodomanda=null){
        $num = $this->getLastQuestionNumber($idq)[0] + 1;
        if($avanzata == "si"){
            if($sottodomanda != null){
                if(isset($na) && $na != null){
                    $query = "INSERT INTO domanda VALUES(?,?,?,CURRENT_DATE(),NULL,?,?)";
                    $stmt = $this->db->prepare($query);
                    $stmt->bind_param("iisii",$idq,$num,$domanda,$na,$sottodomanda);
                }else{
                    $query = "INSERT INTO domanda VALUES(?,?,?,CURRENT_DATE(),NULL,NULL,?)";
                    $stmt = $this->db->prepare($query);
                    $stmt->bind_param("iisi",$idq,$num,$domanda,$sottodomanda);
                }
            }else{
                if(isset($na) && $na != null){
                    $query = "INSERT INTO domanda VALUES(?,?,?,CURRENT_DATE(),NULL,?,NULL)";
                    $stmt = $this->db->prepare($query);
                    $stmt->bind_param("iisi",$idq,$num,$domanda,$na);
                }else{
                    $query = "INSERT INTO domanda VALUES(?,?,?,CURRENT_DATE(),NULL,NULL,NULL)";
                    $stmt = $this->db->prepare($query);
                    $stmt->bind_param("iis",$idq,$num,$domanda);
                    }
            }
        }else{
            $base = 1;
            if($sottodomanda != null){
                if(isset($na)){
                    $query = "INSERT INTO domanda VALUES(?,?,?,CURRENT_DATE(),?,?,?)";
                    $stmt = $this->db->prepare($query);
                    $stmt->bind_param("iisiii",$idq,$num,$domanda,$base,$na,$sottodomanda);
                }else{
                    $query = "INSERT INTO domanda VALUES(?,?,?,CURRENT_DATE(),?,NULL,?)";
                    $stmt = $this->db->prepare($query);
                    $stmt->bind_param("iisii",$idq,$num,$domanda,$base,$sottodomanda);
                }
            }else{
                if(isset($na)){
                    $query = "INSERT INTO domanda VALUES(?,?,?,CURRENT_DATE(),?,?,NULL)";
                    $stmt = $this->db->prepare($query);
                    $stmt->bind_param("iisii",$idq,$num,$domanda,$base,$na);
                }else{
                    $query = "INSERT INTO domanda VALUES(?,?,?,CURRENT_DATE(),?,NULL,NULL)";
                    $stmt = $this->db->prepare($query);
                    $stmt->bind_param("iisi",$idq,$num,$domanda,$base);
                }
            }
        }
        $stmt->execute();
        return $num;
    }

    //DATA UNA DOMANDA E UNA TEMATICA LE AGGIUNGE NELLA TABELLA SVILUPPA
    public function linkQuestionToTheme($idq,$numdomanda,$idt,$peso){
        $query = "INSERT INTO sviluppa VALUES(?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("iiii",$idt,$idq,$numdomanda,$peso);
        $stmt->execute();
    }

    //AGGIUNGE UN CONSIGLIO E RESTITUISCE L'ID  
    public function addAdvice($testo){
        $query = "INSERT INTO consiglio VALUES(NULL, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("s",$testo);
        $stmt->execute();

        $query2 = "SELECT LAST_INSERT_ID()";
        $stmt2 = $this->db->prepare($query2);
        $stmt2->execute();
        return $stmt2->get_result()->fetch_row();
    }

    //AGGIUNGE UN BENEFICIO E RESTITUISCE L'ID  
    public function addBenefit($testo){
        $query = "INSERT INTO beneficio VALUES(NULL,?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("s",$testo);
        $stmt->execute();

        $query2 = "SELECT LAST_INSERT_ID()";
        $stmt2 = $this->db->prepare($query2);
        $stmt2->execute();
        return $stmt2->get_result()->fetch_row()[0];
    }

    //RESTITUISCE LE TEMATICHE REGISTRATE
    public function getAllThemes(){
        $query = "SELECT * FROM tematica";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    }

    //DATA UNA DOMANDA E UNA RISPOSTA LA AGGIUNGE
    public function addAnswer($idq,$numdomanda,$numrisposta,$punteggio = null,$testo){
        if($punteggio != null){
            $query = "INSERT INTO risposta VALUES(?, ?, ?, NULL, ?, ?)";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param("iisii",$numrisposta,$punteggio,$testo,$idq,$numdomanda);
        }else{
            $query = "INSERT INTO risposta VALUES(?, 0, ?, NULL, ?, ?)";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param("isii",$numrisposta,$testo,$idq,$numdomanda);
        }
        $stmt->execute();
        $query2 = "SELECT LAST_INSERT_ID()";
        $stmt2 = $this->db->prepare($query2);
        $stmt2->execute();
        return $stmt2->get_result()->fetch_row();
    }

    //DATA UNA RISPOSTA E BENEFICIO LI COLLEGA
    public function linkAnswerBenefit($idb,$idr){
        $query = "INSERT INTO beneficio_per_risposta VALUES(?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ii",$idb,$idr);
        $stmt->execute();
    }

    //DATA UNA RISPOSTA E CONSIGLIO LI COLLEGA
    public function linkAnswerAdvice($idc,$idr){
        $query = "INSERT INTO visualizza VALUES(?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ii",$idc,$idr);
        $stmt->execute();
    }

    //RESTITUISCE TUTTI GLI ID DEI QUESTIONARI NON COMPLETATI
    public function getUpdatableIds(){
        $query = "SELECT Id, Nome FROM questionario WHERE Completato IS NULL";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    }

    //RESTITUISCE TUTTI GLI ID DEI QUESTIONARI NON COMPLETATI
    public function getCompletedIds(){
        $query = "SELECT Id, Nome FROM questionario WHERE Completato IS NOT NULL";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    }

    //CREA UN NUOVO QUESTIONARIO
    public function createQuest($nome){
        $query = "INSERT INTO questionario VALUES(NULL, ?, NULL)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("s",$nome);
        $stmt->execute();
    }

    //SEGNA UN QUESTIONARIO COME COMPLETATO
    public function completeQuest($idq){
        $query = "UPDATE questionario SET Completato = 1 WHERE Id = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i",$idq);
        $stmt->execute();
    }

    //CANCELLA UN QUESTIONARIO
    public function deleteQuest($idq){
        $query = "DELETE FROM questionario WHERE Id = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i",$idq);
        $stmt->execute();
    }

    //RESTITUISCE PER LA RISPOSTA LA PERCENTUALE DI VOLTE CHE E' STATA SCELTA
    public function getAnswerPercentage($idq,$idr){
        $query = "SELECT COUNT(*) FROM riferisce C, risposta R
        WHERE C.Id_Risposta = ? AND R.Id = C.Id_Risposta AND R.Id_Questionario = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ii",$idr,$idq);
        $stmt->execute();
        $x = $stmt->get_result()->fetch_row()[0];
        $query2 = "SELECT COUNT(*) FROM compilazione C WHERE C_Q_Id = ?";
        $stmt2 = $this->db->prepare($query2);
        $stmt2->bind_param("i",$idq);
        $stmt2->execute();
        $n = $stmt2->get_result()->fetch_row()[0];
        return $x > 0 ? $x / $n * 100 : 0;
    }

    //RESTITUISCE LE COMPILAZIONI TOTALI
    public function getTotalCompilations($idq){
        $query = "SELECT COUNT(*) FROM compilazione WHERE C_Q_Id = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i",$idq);
        $stmt->execute();
        $n = $stmt->get_result()->fetch_row()[0];
        return isset($n) ? $n : 0;
    }

    //RESTITUISCE PER LA RISPOSTA DATA QUANTE VOLTE E' STATA SCELTA NELLA COMPILAZIONE
    public function getAnswerQuantity($idq,$idr){
        $query = "SELECT COUNT(*) FROM riferisce C, risposta R
        WHERE C.Id_Risposta = ? AND R.Id = C.Id_Risposta AND R.Id_Questionario = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ii",$idr,$idq);
        $stmt->execute();
        $x = $stmt->get_result()->fetch_row()[0];
        return $x;
    }

    /*

    Grafico

    */

    //RESTITUISCE SE UN UTENTE ABBIA COMPILATO ALMENO UNA VOLTA IL QUESTIONARIO
    public function checkUserCompiled($idq,$user){
        $query = "SELECT * FROM compilazione WHERE C_Q_Id = ? AND Username = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("is",$idq,$user);
        $stmt->execute();
        return count($stmt->get_result()->fetch_all(MYSQLI_ASSOC)) > 0;
    }

    //RESTITUISCE SE PER IL DATO QUARTIERE E' STATA FATTA ALMENO UNA COMPILAZIONE
    public function checkNeighboorhoodCompiled($idq,$quartiere){
        $query = "SELECT * FROM compilazione C,utente U WHERE C_Q_Id = ? 
        AND C.Username = U.Username AND U.Id_Quartiere = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ii",$idq,$quartiere);
        $stmt->execute();
        return count($stmt->get_result()->fetch_all(MYSQLI_ASSOC)) > 0;
    }

    //RESTITUISCE LA PERCENTUALE DI PUNTEGGIO PER TEMATICA PER L'UTENTE
    public function getThemePercentagePerUser($idq,$user,$tema){
        $query2 = "SELECT Max(Data_Compilazione) FROM compilazione WHERE C_Q_Id = ? AND Username = ?";
        $stmt2 = $this->db->prepare($query2);
        $stmt2->bind_param("is",$idq,$user);
        $stmt2->execute();
        $data = $stmt2->get_result()->fetch_row()[0];
        $query = "SELECT R.Punteggio, S.Peso FROM Compilazione C, Riferisce RI, Risposta R, Domanda D, Sviluppa S
        WHERE C.Username = ? AND C_Q_Id = ? AND RI.Id_Compilazione = C.Id AND RI.Id_Risposta = R.Id
        AND D.Num = R.Num AND D.Num = S.Num AND S.Id_Tematica = ? AND C.Data_Compilazione = ? GROUP BY R.Id";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("siis",$user,$idq,$tema,$data);
        $stmt->execute();
        $result = $stmt->get_result();
        $x = 0;
        foreach($result->fetch_all(MYSQLI_ASSOC) as $r){
            $x += $r["Punteggio"] * $r["Peso"];
        }
        return $x > 0 ? $x / $this->getTotalPointForTheme($idq,$tema) * 100 : 0;
    }

    //RESTITUISCE LA PERCENTUALE DI PUNTEGGIO PER TEMATICA PER L'UTENTE PER LA PENULTIMA COMPILAZIONE
    public function getThemePercentagePerUserBefore($idq,$user,$tema){
        $query2 = "SELECT Data_Compilazione FROM compilazione WHERE C_Q_Id = ? AND Username = ? 
        ORDER BY Data_Compilazione DESC LIMIT 2";
        $stmt2 = $this->db->prepare($query2);
        $stmt2->bind_param("is",$idq,$user);
        $stmt2->execute();
        $result = $stmt2->get_result()->fetch_all(MYSQLI_ASSOC);
        if(count($result) > 1){
            $data = $result[1]["Data_Compilazione"];
        }
        if(!isset($data)){
            return -1;
        }
        $query = "SELECT R.Punteggio, S.Peso FROM Compilazione C, Riferisce RI, Risposta R, Domanda D, Sviluppa S
        WHERE C.Username = ? AND C_Q_Id = ? AND RI.Id_Compilazione = C.Id AND RI.Id_Risposta = R.Id
        AND D.Num = R.Num AND D.Num = S.Num AND S.Id_Tematica = ? AND C.Data_Compilazione = ? GROUP BY R.Id";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("siis",$user,$idq,$tema,$data);
        $stmt->execute();
        $result = $stmt->get_result();
        $x = 0;
        foreach($result->fetch_all(MYSQLI_ASSOC) as $r){
            $x += $r["Punteggio"] * $r["Peso"];
        }
        return $x > 0 ? $x / $this->getTotalPointForTheme($idq,$tema) * 100 : 0;
    }

    //CONTROLLA CHE L'UTENTE ABBIA FATTO ALMENO UNA COMPILAZIONE PER IL QUESTIONARIO SCELTO
    public function checkCompiledOnce($idq,$user){
        $query = "SELECT * FROM utente u, compilazione c WHERE u.username = c.username AND
        u.username = ? AND c.C_Q_Id = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("si",$user,$idq);
        $stmt->execute();
        $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        return (count($result) > 0);
    }

    //RESTITUISCE LA PERCENTUALE MEDIA DI PUNTEGGIO PER TEMATICA
    public function getThemePercentageGlobal($idq,$tema){
        $query = "SELECT Username FROM utente";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        $x = 0; //Punteggio parziale
        $n = 0; //Numero utenti che hanno compilato
        foreach($result as $r){
            if($this->checkCompiledOnce($idq,$r["Username"])){
                $v = $this->getThemePercentagePerUser($idq,$r["Username"],$tema);
                $x += $v;
                $n++;
            }
        }
        return $x > 0 ? $x / $n : 0;
    }

    //RESTITUISCE LA PERCENTUALE MEDIA DI PUNTEGGIO PER TEMATICA, PER QUARTIERE
    public function getThemePercentageNeighboorhood($idq,$tema,$quartiere){
        $query = "SELECT Username FROM utente WHERE Id_Quartiere = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i",$quartiere);
        $stmt->execute();
        $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        $x = 0; //Punteggio parziale
        $n = 0; //Numero utenti che hanno compilato
        foreach($result as $r){
            if($this->checkCompiledOnce($idq,$r["Username"])){
                $v = $this->getThemePercentagePerUser($idq,$r["Username"],$tema);
                $x += $v;
                $n++;
            }
        }
        return $x > 0 ? $x / $n : 0;
    }

    //RESTITUISCE IL PUNTEGGIO MASSIMO PER TEMATICA
    public function getTotalPointForTheme($idq,$tema){
        $query = "SELECT D.Num, MAX(R.Punteggio) AS Punteggio, S.Peso FROM Risposta R, Domanda D, Sviluppa S
        WHERE D.Id_Questionario = ?
        AND D.Num = R.Num AND D.Num = S.Num AND S.Id_Tematica = ?
        GROUP BY D.Num";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ii",$idq,$tema);
        $stmt->execute();
        $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        $t = 0;
        foreach($result as $r){
            $t += $r["Punteggio"] * $r["Peso"];
        }
        return $t;
    }

}

?>