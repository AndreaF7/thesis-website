<?php
require_once 'bootstrap.php';
if(!isset($_SESSION["username"]) || empty($dbh->checkAdmin($_SESSION["username"]))){
    header("Location: index.php");
}else{
    if(isset($_POST) && count($_POST) > 0){
        if(isset($_POST["nomeq"]) && $_POST["nomeq"] != ""){
            $dbh->createQuest($_POST["nomeq"]);
            $tp["successo"] = 1;
        }else{
            $tp["successo"] = 0;
        }
    }
    $_SESSION["Idq"] = 1;
    $tp["titolo"] = "Admin";
    $tp["nome"] = "template/admin.php";
    $tp["header"] = 3;
}
require 'template/base.php';
?>