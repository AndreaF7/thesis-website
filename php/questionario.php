<?php
require_once 'bootstrap.php';
if(!isset($_SESSION["username"])){
    header("Location: index.php");
}else{
    //Invio questionario
    if(isset($_POST) && count($_POST) > 0){
        if(isset($_POST["idq"])){
            $_SESSION["Idq"] = $_POST["idq"];
        }else{
            $domande = $dbh->getMainQuestions($_SESSION["Idq"]);
            $ndomande = 0;
            if($_POST["avanzate"] == 0){
                foreach($domande as $domanda){
                    if($domanda["Base"] == 1){
                        $ndomande++;
                    }
                }
            }else{
                $ndomande = count($domande);
            }
            array_shift($_POST);
            $n = 0;
            foreach($_POST as $k => $x){
                if($k[0] != "c" && !$dbh->checkSubQuestion(ltrim($k,"d"),$_SESSION["Idq"])){
                    $n++;
                }
            }
            if($ndomande == $n){
                $idc = $dbh->compileForm($_SESSION["username"],$_SESSION["Idq"])[0];
                if($idc != 0){
                    foreach($_POST as $k => $x){
                        //$nd = ltrim($k,$k[0]);
                        if($k[0] == "c" && $x == "Si"){
                            //Aggiunge la conferma del beneficio
                            $nr = substr($k,strpos($k,"c") + 1,strpos($k,"-") - 1);
                            $nb = substr($k,strpos($k,"b") + 1,strpos($k,"-") - 1);
                            $riga = $dbh->getBenefitConfirm($_SESSION["username"], $nr, $nb);
                            if(!empty($riga)){
                                $riga = $riga[0];
                                $dbh->updateBenefitConfirm($riga["Username"], $riga["Id_Risposta"], $riga["Id_Beneficio"], 1);
                            }else{
                                $dbh->sendBenefitConfirm($_SESSION["username"], $nr, $nb);
                            }
                        }elseif($k[0] == "c" && $x == "No"){
                            //Controlla se ha già confermato per cambiare la conferma
                            $nr = substr($k,strpos($k,"c") + 1,strpos($k,"-") - 1);
                            $nb = substr($k,strpos($k,"b") + 1,strpos($k,"-") - 1);
                            $riga = $dbh->getBenefitConfirm($_SESSION["username"], $nr, $nb);
                            if(!empty($riga)){
                                $riga = $riga[0];
                                $dbh->updateBenefitConfirm($riga["Username"], $riga["Id_Risposta"], $riga["Id_Beneficio"], 0);
                            }
                        }else {
                            $dbh->sendAnswer($idc, $x);
                        }
                    }
                    $tp["successo"] = 1;
                }else{
                    $tp["successo"] = -1;
                }
            }else{
                $tp["successo"] = 0;
            }
        }
    }

    $tp["titolo"] = "Questionario";
    $tp["nome"] = "template/questionario-form.php";
    $tp["header"] = 1;
    $tp["avanzate"] = 0;
}
require "template/base.php";
?>
