<!DOCTYPE html>
<html lang="it">
<head>
  <title><?php echo $tp["titolo"]; ?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <link rel="stylesheet" href="../css/style.css">
  <script src="../js/questionario.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/crypto-js.min.js"></script>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script src="../js/profiling.js"></script>
  <script src="../js/admin.js"></script>
</head>
<body>
    <header>
        <?php
            if(isset($tp["header"])){
                if($tp["header"] == 1){
                    echo '<div class="up"><a href="profilo.php">Profilo</a></div>';
                    echo '<div class="up"><a href="logout.php">Scollegati</a></div>';
                }else if($tp["header"] == 2){
                    echo '<div class="up"><a href="index.php">Home</a></div>';
                }else if($tp["header"] == 3){
                    echo '<div class="up"><a href="index.php">Home</a></div>';
                    echo '<div class="up"><a href="logout.php">Scollegati</a></div>';
                }
                if(isset($_SESSION) && count($_SESSION) > 0 && count($dbh->checkAdmin($_SESSION["username"])) > 0){
                    echo '<div class="up"><a href="admin.php">Admin</a></div>';
                }
                if(isset($_SESSION["Idq"]) && $dbh->checkCompiledOnce($_SESSION["Idq"],$_SESSION["username"])){
                    echo '<div class=up><a href=terminale.php>Statistiche</a></div>';
                }
            }
            ?>
    </header>
    <main>
        <?php
            if(isset($tp["nome"])){
                require($tp["nome"]);
            }
            ?>
    </main>
    <footer></footer>
</body>
</html>