<section>
      <div class="upper-text">
        <h2>Benvenuto</h2>
      </div>
      <form class="middle-text profiling" method="POST" id="form-login" >
        <div class="text-and-input-container">
          <label for="username">Username
            <input type="text" name="username" placeholder="Username"/>
          </label>
        </div>
        <div class="text-and-input-container">
          <label for="password">Password
            <input type="password" name="password" placeholder="Password"/>
          </label>
        </div>
        <input type="submit" value="Entra"/>
        </form>
        <h3 class="dati-errati">
          <?php
            if(isset($tp["erroreLogin"])){
              switch($tp["erroreLogin"]){
                case 1:
                  echo "Username o password non inseriti";
                break;
                case 2:
                  echo "Username o password errati";
              }
            }
          ?>
        </h3>
        <div class="lower-text">
          <h3 id="not-registered">Non sei registrato?</h3>
          <a href="registrazione.php">Registrati</a>
        </div>
    </section>