<section>
    <?php
        echo '<h3>Questionario '.$_SESSION["Idq"].'</h3>';
        echo '<h4>Compilazioni totali: '.$dbh->getTotalCompilations($_SESSION["Idq"]).'</h4>';
    ?>
    <div id="contenitore-domande">
        <?php
            foreach($dbh->getQuestions($_SESSION["Idq"]) as $domanda){
                if($domanda["Id_Sottodomanda"] == NULL){
                    echo '<ul>'.$domanda["Testo"];
                    if($domanda["Base"] != 1){
                        echo ' [Domanda avanzata]';
                    }
                    foreach($dbh->getQuestionAnswers($domanda["Num"],$_SESSION["Idq"]) as $risposta){
                        echo '<li>'.$risposta["Testo"].'&nbsp;&nbsp;&nbsp;&nbsp;
                            '.number_format($dbh->getAnswerPercentage($_SESSION["Idq"],$risposta["Id"]),2).'%</li>';
                        $benefici = $dbh->getAnswerBenefits($risposta["Id"]);
                        $sottodomande = $dbh->getSubQuestion($risposta["Id"]);
                        if(count($benefici) > 0) {
                            echo '<div>
                            <ul class="beneficio">Benefici ottenuti:';
                            foreach($benefici as $beneficio){
                                echo '<li>'.$beneficio["Testo"].'</li>';
                                echo '<label for="conferma-beneficio" class="conferma">'.number_format($dbh->getBenefitPercentage($beneficio["Id"]),2).'% utenti confermano</label>';
                            }
                            echo '</ul></div>';
                        }else if(count($sottodomande) > 0){
                            echo '<div>';
                            foreach($sottodomande as $sd){
                                echo '<ul>'.$sd["Testo"];
                                foreach($dbh->getQuestionAnswers($sd["Num"],$_SESSION["Idq"]) as $sr){
                                    echo '<li>'.$sr["Testo"].'&nbsp;&nbsp;&nbsp;&nbsp;     
                                    '.number_format($dbh->getAnswerPercentage($_SESSION["Idq"],$sr["Id"]),2).'%</li>';
                                }
                            }
                            echo '</div>';
                        }
                    }
                    echo '</ul>';
                }
            }
        ?>
    </div>
</section>