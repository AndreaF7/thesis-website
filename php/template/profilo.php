<section>
  <div class="upper-text">
    <?php
    if(isset($tp["erroreModifica"]) && $tp["erroreModifica"] == 0){
      echo '<h3 class="successo">'.$tp["notifica"].'</h3>';
    }else if(isset($tp["erroreModifica"])){
      echo '<h3 class="fallimento">'.$tp["notifica"].'</h3>';
    }
    ?>
    <p>In questa pagina puoi modificare la password o il quartiere di residenza</p>
  </div>
  <form class="middle-text" id="cambiadati" method="POST">
  <div class="text-and-input-container">
      <label for="vecchia-password">Vecchia Password
        <input type="password" name="vecchia-password" placeholder="Vecchia Password"/>
      </label>
    </div>
    <div class="text-and-input-container">
      <label for="password">Password
        <input type="password" name="password" placeholder="Password"/>
      </label>
    </div>
    <div class="text-and-input-container">
      <label for="conferma-password">Conferma Password
        <input type="password" name="conferma-password" placeholder="Conferma Password"/>
      </label>
    </div>
    <div class="text-and-input-container">
      <label for="quartiere">Quartiere</label>
      <br>
      <label for="cesena">Dentro Cesena</label>
      <input type="checkbox" id="cesena" name="cesena" value="si"/>
      <br>
        <select name="quartiere" disabled>
          <?php
              foreach($dbh->getPlaces() as $quartiere){
                if($quartiere["Id"] != $_SESSION["quartiere"]){
                  echo '<option value="q' . $quartiere["Id"] .'">' . $quartiere["Nome"] . '</option>';
                }else{
                  echo '<option value="q' . $quartiere["Id"] .'" selected>' . $quartiere["Nome"] . '</option>';
                }
            }
          ?>
        </select> 
    </div>
    <input type="submit" value="Cambia"/>
  </form>
</section>