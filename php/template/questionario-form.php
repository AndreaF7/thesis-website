<section>
    <?php    
    if(isset($tp["successo"])){
        if($tp["successo"] == 1){
            $_SESSION["inviato"] = 1;
            header("Location: terminale.php");
        }elseif($tp["successo"] == -1){
            echo '<h3 class=fallimento>Questionario già inviato oggi</h3>';
        }else{
            echo '<h3 class="fallimento">Non hai risposto a tutte le domande</h3>';
        }
    }
    ?>
    <div id="contenitore-domande-avanzate">
        <a id="domande-avanzate" href=<?php
        if($tp["avanzate"] == 0){
            echo '"avanzate.php"';
        }else{
            echo '"questionario.php"';
        }
        ?>>
            <?php if($tp["avanzate"] == 0){
                echo "Vedi domande avanzate";
            }else{
                echo "Torna alle domande base";
            }?>
        </a>
    </div>
    <?php
    if(count($dbh->getCompletedIds()) > 0){
        echo '<form method=POST action=#>';
        echo '<select name=idq>';
        foreach($dbh->getCompletedIds() as $idq){
            if($idq["Id"] == $_SESSION["Idq"]){
                echo '<option value='.$idq["Id"].' selected>Questionario "'.$idq["Nome"].'"</option>';
            }else{
                echo '<option value='.$idq["Id"].'>Questionario "'.$idq["Nome"].'"</option>';
            }
        }
        echo '</select><br><br>';
        echo '<input type=submit value=Cambia />';
        echo '</form>';
    }
    ?>
    <div id="contenitore-domande">
        <form method="POST" action="questionario.php">
            <?php
                echo '<input type="hidden" id="input-avanzate" name="avanzate" value="'.$tp["avanzate"].'">';
                foreach($dbh->getQuestions($_SESSION["Idq"]) as $domanda){
                    if(($tp["avanzate"] == 0 && $domanda["Base"] == 1 || $tp["avanzate"] == 1) && $domanda["Id_Sottodomanda"] == NULL){
                        echo '<fieldset><label id="domanda'.$domanda["Num"].'">'.$domanda["Testo"].'</label>';
                        if(isset($domanda["Id_Approfondimento"])){
                            //Approfondimento
                            echo '<a class="tasto-approfondimento" id="tasto-approfondimento-domanda'.$domanda["Num"].'">?</a>
                            <div class="finestra-approfondimento" id="finestra-approfondimento-domanda'.$domanda["Num"].'">
                                <div class="finestra-approfondimento-contenuto">
                                    <span class="chiudi">&times;</span>
                                    <p>'.$dbh->getQuestionInfo($domanda["Num"],$_SESSION["Idq"])[0]["Testo"].'</p>';
                                    if(isset($dbh->getQuestionInfo($domanda["Num"],$_SESSION["Idq"])[0]["Link"])){
                                        echo '<a href='.$dbh->getQuestionInfo($domanda["Num"],$_SESSION["Idq"])[0]["Link"].'>Link esterno</a>';
                                    }
                                    echo '
                                </div>
                            </div>';
                            //Fine Approfondimento
                        }
                        echo '<br>';
                        foreach($dbh->getQuestionAnswers($domanda["Num"],$_SESSION["Idq"]) as $risposta){
                            echo '<input type="radio" id="r'.$risposta["Id"].'" name="d'.$domanda["Num"].'" value="'.$risposta["Id"].'">
                            <label for="r'.$risposta["Id"].'">'.$risposta["Testo"].'</label><br>';
                            $benefici = $dbh->getAnswerBenefits($risposta["Id"]);
                            $consigli = $dbh->getAnswerAdvices($risposta["Id"]);
                            $sottodomande = $dbh->getSubQuestion($risposta["Id"]);
                            if(count($benefici) > 0) {
                                echo '<div class="beneficio">
                                <ul class="beneficio">Benefici ottenuti:';
                                foreach($benefici as $beneficio){
                                    echo '<li>'.$beneficio["Testo"].'</li>';
                                    echo '<label for="conferma-beneficio" class="conferma">'.$dbh->getBenefitPercentage($beneficio["Id"]).'% utenti confermano, confermi?</label>
                                    <input type="radio" class="radio-beneficio" id="b'.$beneficio["Id"].'si" name="c'.$risposta["Id"].'-b'.$beneficio["Id"].'"
                                    value="Si">
                                    <label for="b'.$beneficio["Id"].'si">Si</label>
                                    <input type="radio" class="radio-beneficio" id="b'.$beneficio["Id"].'no" name="c'.$risposta["Id"].'-b'.$beneficio["Id"].'"
                                    value="No">
                                    <label for="b'.$beneficio["Id"].'no">No</label>
                                    <input type="radio" class="radio-beneficio" id="b'.$beneficio["Id"].'non-saprei" name="c'.$risposta["Id"].'-b'.$beneficio["Id"].'"
                                    value="No">
                                    <label for="b'.$beneficio["Id"].'non-saprei">Non saprei</label>';
                                }
                                echo '</ul></div>';
                            }else if(count($consigli) > 0){
                                echo '<div class="consiglio">';
                                echo '<ul class="consiglio">Consigli:';
                                foreach($consigli as $consiglio){
                                    echo '<li>'.$consiglio["Testo"].'</li>';
                                }
                                echo '</ul></div>';
                            }else if(count($sottodomande) > 0){
                                echo '<fieldset class=sottodomanda>';
                                foreach($sottodomande as $sd){
                                    echo '<label id=domanda'.$sd["Num"].'>'.$sd["Testo"].'</label>';
                                    echo '<br>';
                                    foreach($dbh->getQuestionAnswers($sd["Num"],$_SESSION["Idq"]) as $sr){
                                        echo '<input type="radio" id="r'.$sr["Id"].'" name="d'.$sd["Num"].'" value="'.$sr["Id"].'">
                                        <label for="r'.$sr["Id"].'">'.$sr["Testo"].'</label><br>';
                                    }
                                }
                                echo '</fieldset>';
                            }
                        }
                        echo '</fieldset>';
                    }
                }
            ?>
            <input type="submit"/>
        </form>
    </div>
</section>