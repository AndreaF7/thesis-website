<section>
        <div class="register-container">
        <form class="middle-text" method="POST" id="form-registrazione">
          <div class="text-and-input-container">
            <label for="username">Username (*)
              <input type="text" name="username" placeholder="Username"/>
            </label>
          </div>
          <div class="text-and-input-container" class="password-and-confirm">
            <label for="password">Password (*)
              <input type="password" name="password" placeholder="Password"/>
            </label>
            <label for="conferma-password">Conferma Password (*)
              <input type="password" name="conferma-password" placeholder="Conferma Password"/>
            </label>
          </div>
          <div class="text-and-input-container">
            <label for="email">Email
              <input type="email" name="email" placeholder="Email"/>
            </label>
          </div>
          <div class="text-and-input-container">
          <label for="quartiere">Quartiere</label>
      <br>
      <label for="cesena2">Dentro Cesena</label>
      <input type="checkbox" id="cesena2" name="cesena" value="si"/>
      <br>
        <select name="quartiere" disabled>
          <?php
              foreach($dbh->getPlaces() as $quartiere){
                if($quartiere["Id"] != $_SESSION["quartiere"]){
                  echo '<option value="q' . $quartiere["Id"] .'">' . $quartiere["Nome"] . '</option>';
                }else{
                  echo '<option value="q' . $quartiere["Id"] .'" selected>' . $quartiere["Nome"] . '</option>';
                }
            }
          ?>
        </select> 
          </div>
          <input type="submit" value="Registrati"/>
          </form>
          <?php
            if(isset($tp["erroreRegistrazione"])){
              if($tp["erroreRegistrazione"] == 1){
                echo '<h3 class="dati-errati">Username o password non inseriti</h3>';
              }else if($tp["erroreRegistrazione"] == 2){
                echo '<h3 class="dati-errati">Username già presente</h3>';
              }else if($tp["erroreRegistrazione"] == 3){
                echo '<h3 class="dati-errati">Le due password non combaciano</h3>';
              }
            }
          ?>
        </div>
      </section>