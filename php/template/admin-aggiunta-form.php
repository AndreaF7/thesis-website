<section>
<?php    
    if(isset($tp["successo"])){
        if($tp["successo"] == 1){
          echo '<h3 class="successo">Domanda aggiunta</h3>';
        }elseif($tp["successo"] == 0){
          echo '<h3 class="fallimento">Mancano i campi obbligatori</h3>';
        }elseif($tp["successo"] == -1){
          echo '<h3 class="fallimento">Questionario già completato</h3>';
        }
    }
    ?>
  <form method="POST" action="#">
    <label for="aggiungi-domanda">Domanda: (*)</label><br>
    <input type="text" id="aggiungi-domanda" name="aggiungi-domanda" required><br><br>
    <label for="avanzata">Domanda avanzata?</label>
    <input type="checkbox" id="avanzata" name="avanzata" value="si"><br>
    <div>Selezionare il peso per le tematiche che sviluppa (*)
      <br>
      <?php
        foreach($dbh->getAllThemes() as $tematica){
          echo '<label for=t'.$tematica["Id"].'>'.$tematica["Tema"].'</label> ';
          echo '<input type=number id=t'.$tematica["Id"].' name=t'.$tematica["Id"].' min=0 max=10>';
        }
      ?>
    </div>
    <br>
    <div id="risposte">
      <div>
      <label for="aggiungi-risposta1">Risposta 1: (*)</label>
      <input type="text" id="aggiungi-risposta1" name="aggiungi-risposta1" required>
      <label for="punteggio-r1">Punteggio: (*)</label>
      <input type="number" id="punteggio-r1" name="punteggio-r1" min="0" max="10" required>
      <br>
      <label for="aggiungi-beneficio-r1">Aggiungi beneficio</label>
      <input type="button" name="aggiungi-beneficio-r1" value="+">
      <input type="button" name="rimuovi-beneficio-r1" value=->
      <label for="aggiungi-consiglio-r1">Aggiungi consiglio</label>
      <input type="button" name="aggiungi-consiglio-r1" value="+">
      <input type="button" name="rimuovi-consiglio-r1" value=->
      <label for="aggiungi-sottodomanda-r1">Aggiungi sottodomanda</label>
      <input type="button" name="aggiungi-sottodomanda-r1" value="+">
      <input type="button" name="rimuovi-sottodomanda-r1" value=->
      </div>
      <br>
      <div>
      <label for="aggiungi-risposta2">Risposta 2: (*)</label>
      <input type="text" id="aggiungi-risposta2" name="aggiungi-risposta2" required>
      <label for="punteggio-r2">Punteggio: (*)</label>
      <input type="number" id="punteggio-r2" name="punteggio-r2" min="0" max="10" required>
      <br>
      <label for="aggiungi-beneficio-r2">Aggiungi beneficio</label>
      <input type="button" name="aggiungi-beneficio-r2" value="+">
      <input type="button" name="rimuovi-beneficio-r2" value=->
      <label for="aggiungi-consiglio-r2">Aggiungi consiglio</label>
      <input type="button" name="aggiungi-consiglio-r2" value="+">
      <input type="button" name="rimuovi-consiglio-r2" value=->
      <label for="aggiungi-sottodomanda-r2">Aggiungi sottodomanda</label>
      <input type="button" name="aggiungi-sottodomanda-r2" value="+">
      <input type="button" name="rimuovi-sottodomanda-r2" value=->
      </div>
      <br>
    </div>
    <label for="agg-risp">Aggiungi risposta</label>
    <input type="button" name="agg-risp" value="+">
    <input type="button" name="rim-risp" value="-">
    <br>
    <label for="aggiungi-approfondimento">Approfondimento:</label><br>
    <textarea id="aggiungi-approfondimento" name="aggiungi-approfondimento" placeholder="Opzionale"></textarea>
    <br>
    <label for="aggiungi-approfondimento-link">Link esterno:</label><br>
    <input type="text" id="aggiungi-approfondimento-link" name="aggiungi-approfondimento-link" placeholder="Opzionale"><br>
    <br>
    <input type="submit" value="Aggiungi"/>
  </form>
</section>