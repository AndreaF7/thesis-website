<section class="home-section">
<?php    
    if(isset($tp["successo"])){
        if($tp["successo"] == 1){
          echo '<h3 class="successo">Nuovo questionario creato</h3>';
        }elseif($tp["successo"] == 0){
          echo '<h3 class="fallimento">Nome non inserito</h3>';
        }
    }
    ?>
  <form method="POST" action="#">
    <label for="nomeq">Nome questionario:</label><br>
    <input type="name" id="nomeq" name="nomeq" /><br>
    <input type="submit" value="Crea" />
  </form>
  <br><br>
    <?php
    if(count($dbh->getUpdatableIds()) > 0){
      echo '<form method=POST action=admin-aggiunta.php>';
      echo '<label for=idq>Modifica:</label>';
      echo '<select name=idq>';
      foreach($dbh->getUpdatableIds() as $idq){
        echo '<option value='.$idq["Id"].'>Questionario "'.$idq["Nome"].'"</option>';
      }
      echo '</select><br><br>';
      echo '<input type=submit value="Aggiungi domanda" />';
      echo '<input type=submit name=completa value=Completa />';
      echo '</form><br>';
    }
    if(count($dbh->getCompletedIds()) > 0){
      echo '<form method=POST action=admin-visualizza.php>';
      echo '<label for=vidq>Visualizza:</label>';
      echo '<select name=vidq>';
      foreach($dbh->getCompletedIds() as $idq){
        echo '<option value='.$idq["Id"].'>Questionario "'.$idq["Nome"].'"</option>';
      }
      echo '</select><br><br>';
      echo '<input type=submit name=visualizza value="Visualizza risposte" />';
      echo '<input type=submit name=cancella value=Elimina />';
      echo '</form>';
    }
    ?>
</section>