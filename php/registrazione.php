<?php
require_once 'bootstrap.php';
if(isset($_POST["username"]) && isset($_POST["password"])){
    $username = $_POST["username"];
    $password = $_POST["password"];
    if($username == "" || $password == ""){
        $tp["erroreRegistrazione"] = 1;
    }else if(count($dbh->checkUser($username)) != 0){
        $tp["erroreRegistrazione"] = 2;
    }else if(!isset($_POST["conferma-password"]) || $password != $_POST["conferma-password"]){
        $tp["erroreRegistrazione"] = 3;
    }else{
        if(isset($_POST["cesena"]) && $_POST["cesena"] == "si"){
            $idquartiere = substr($_POST["quartiere"],1);
        } else{
            $idquartiere = null;
        }
        $_POST["email"] == "" ? $email = NULL : $email = $_POST["email"];
        $dbh->registerUser($username,$password,$email,$idquartiere);
        $tp["erroreRegistrazione"] = 0;
        $_SESSION["username"] = $username;
        $_SESSION["quartiere"] = $idquartiere;
        $_SESSION["Idq"] = 1;
        header("Location: index.php");
    }
}
$tp["titolo"] = "Registrazione";
$tp["nome"] = "template/registrazione-form.php";
$tp["header"] = 2;
require 'template/base.php';
?>