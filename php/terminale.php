<?php
require_once 'bootstrap.php';
if(isset($_SESSION["username"])){
    if(isset($_POST["cambia"]) && count($_POST) > 0){
        $cambia = $_POST["idq"];
        $_SESSION["Idq"] = $cambia;
    }
    $tp["titolo"] = "Consegnato";
    $tp["header"] = 3;
    require "template/base.php";
}else{
    header("Location: questionario.php");
}

?>
<!DOCTYPE html>
<html lang="it">
<head>
  <title><?php echo $tp["titolo"]; ?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://cdn.anychart.com/releases/8.7.1/js/anychart-core.min.js">     </script>
    <script src="https://cdn.anychart.com/releases/8.7.1/js/anychart-radar.min.js"></script>
  <link rel="stylesheet" href="../css/style.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/crypto-js.min.js"></script>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">

    anychart.onDocumentReady(function () {
        <?php
        if($dbh->checkUserCompiled($_SESSION["Idq"], $_SESSION["username"])){
            echo 'var user = [';
                foreach($dbh->getAllThemes() as $tema){
                    echo '{x: "'.$tema["Tema"].'", value: '.$dbh->getThemePercentagePerUser(
                        isset($_SESSION["Idq"]) ? $_SESSION["Idq"] : 1,$_SESSION["username"],$tema["Id"]).'},';
                }
            echo '];';
        }
    ?>

    <?php
    $b = 0;
    foreach($dbh->getAllThemes() as $tema){
        if($dbh->getThemePercentagePerUserBefore(isset($_SESSION["Idq"]) ? $_SESSION["Idq"] : 1,$_SESSION["username"],$tema["Id"]) != -1){
            $b++;
        }
    }
    if($b > 0){
        echo 'var user2 = [';
        foreach($dbh->getAllThemes() as $tema){
            echo '{x: "'.$tema["Tema"].'", value: '.$dbh->getThemePercentagePerUserBefore(
                isset($_SESSION["Idq"]) ? $_SESSION["Idq"] : 1,$_SESSION["username"],$tema["Id"]).'},';
        }
        echo '];';
    }
    ?>

    var global = [
        <?php
            foreach($dbh->getAllThemes() as $tema){
                echo '{x: "'.$tema["Tema"].'", value: '.$dbh->getThemePercentageGlobal(
                    isset($_SESSION["Idq"]) ? $_SESSION["Idq"] : 1,$tema["Id"]).'},';
            }
        ?>
    ];

    var q = [
        <?php
            foreach($dbh->getAllThemes() as $tema){
                echo '{x: "'.$tema["Tema"].'", value: '.$dbh->getThemePercentageNeighboorhood(
                    isset($_SESSION["Idq"]) ? $_SESSION["Idq"] : 1,$tema["Id"],$_SESSION["quartiere"]).'},';
            }
        ?>
    ];

    <?php
    if($dbh->checkAdmin($_SESSION["username"])){
        $quartieri = array();
        foreach($dbh->getPlaces() as $q){
            if($dbh->checkNeighboorhoodCompiled($_SESSION["Idq"],$q["Id"])){
                array_push($quartieri, $q);
                echo 'var q'.$q["Id"].' = [';
                foreach($dbh->getAllThemes() as $t){
                    echo '{x: "'.$t["Tema"].'", value: '.$dbh->getThemePercentageNeighboorhood(
                        isset($_SESSION["Idq"]) ? $_SESSION["Idq"] : 1,$t["Id"],$q["Id"]).'},';
                }
                echo '];';
            }
        }
    }
    ?>
    
    var chart = anychart.radar();

    chart.title("Punteggio per tematica").legend(true);

    chart.yScale()
    .minimum(0)
    .maximum(100)
    .ticks({'interval':20});

    <?php
    if(!$dbh->checkAdmin($_SESSION["username"])){
        echo 'chart.area(q).name("Quartiere").markers(true);';
    }?>

    chart.area(global).name("Media").markers(true);

    <?php
    if($dbh->checkUserCompiled($_SESSION["Idq"],$_SESSION["username"]) && !$dbh->checkAdmin($_SESSION["username"])){
        echo 'chart.area(user).name("Utente, ultima compilazione").markers(true);';
    }
    ?>

    <?php
    if(!$dbh->checkAdmin($_SESSION["username"]) && $dbh->getThemePercentagePerUserBefore(isset($_SESSION["Idq"]) ? $_SESSION["Idq"] : 1,$_SESSION["username"],$tema["Id"]) != -1){
        echo 'chart.area(user2).name("Utente, penultima compilazione").markers(true)';
    }
    ?>

    <?php
    if($dbh->checkAdmin($_SESSION["username"]) && count($quartieri) > 0){
        foreach($quartieri as $q){
            echo 'chart.area(q'.$q["Id"].').name("'.$q["Nome"].'").markers(true);';
        }
    }
    ?>

    chart.container('chart-container');

    chart.draw();

    $(".anychart-credits").hide();
        });


  </script>
</head>
<body>
    <header>
        <?php
            if(isset($tp["header"])){
                if($tp["header"] == 1){
                    echo '<div class="up"><a href="profilo.php">Profilo</a></div>';
                    echo '<div class="up"><a href="logout.php">Scollegati</a></div>';
                }else if($tp["header"] == 2){
                    echo '<div class="up"><a href="index.php">Home</a></div>';
                }else if($tp["header"] == 3){
                    echo '<div class="up"><a href="index.php">Home</a></div>';
                    echo '<div class="up"><a href="logout.php">Scollegati</a></div>';
                }
                if(isset($_SESSION) && count($_SESSION) > 0 && count($dbh->checkAdmin($_SESSION["username"])) > 0){
                    echo '<div class="up"><a href="admin.php">Admin</a></div>';
                }
            }
            ?>
    </header>
    <main>
        <section id="terminal">
            <?php
            if(isset($_SESSION["inviato"]) && $_SESSION["inviato"] == 1){
                echo '<h3 class="successo">QUESTIONARIO CORRETTAMENTE INVIATO</h3>
                <br>';
                $_SESSION["inviato"] = 0;
            }
            if(count($dbh->getCompletedIds()) > 0){
                echo '<form method=POST action=#>';
                echo '<select name=idq>';
                foreach($dbh->getCompletedIds() as $idq){
                    if($idq["Id"] == $_SESSION["Idq"]){
                        echo '<option value='.$idq["Id"].' selected>Questionario "'.$idq["Nome"].'"</option>';
                    }else{
                        echo '<option value='.$idq["Id"].'>Questionario "'.$idq["Nome"].'"</option>';
                    }
                }
                echo '</select><br>';
                echo '<input type=submit name=cambia value=Cambia />';
                echo '</form><br>';
            }
            ?>
            <div id="chart-container">
            </div>
        <section>
    </main>
    <footer></footer>
</body>
</html>