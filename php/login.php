<?php
  require_once "bootstrap.php";
  if(isset($_POST["username"]) && isset($_POST["password"])){
    $username = $_POST["username"];
    $password = $_POST["password"];
    if($username == "" || $password == ""){
      $tp["erroreLogin"] = 1;
    }else{
      $loginResult = $dbh->checkLogin($username,$password);
      if(count($loginResult) == 0){
        $tp["erroreLogin"] = 2;
      }else{
        $tp["erroreLogin"] = 0;
        $user = $loginResult[0];
        $_SESSION["username"] = $username;
        $_SESSION["quartiere"] = $user["id_quartiere"];
        $_SESSION["Idq"] = 1;
        header("Location: index.php");
      }
    }
  }
  $tp["titolo"] = "Entra";
  $tp["nome"] = "template/login-form.php";
  $tp["header"] = 2;
  require "template/base.php";
?>