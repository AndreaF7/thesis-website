<?php
require_once 'bootstrap.php';
if(!isset($_SESSION["username"]) || empty($dbh->checkAdmin($_SESSION["username"]))){
    header("Location: index.php");
}else{
    if(isset($_POST) && count($_POST) > 0){
        if(isset($_POST["idq"])){
            $_SESSION["Idq"] = $_POST["idq"];
            if(isset($_POST["completa"])){
                $dbh->completeQuest($_SESSION["Idq"]);
                header("Location: admin.php");
            }
        }else{
            $b = 0;
            foreach($dbh->getUpdatableIds() as $q){
                if($q["Id"] == $_SESSION["Idq"]){
                    $b = 1;
                break;
                }
            }
            if($b == 1){
                $domanda = $_POST["aggiungi-domanda"];
                $r = array();
                $b = array(array());
                $c = array(array());
                $sd = array();
                $idb = array(array());
                $idc = array(array());
                $sdrisposte = array(array());
                $t = array();
                $punteggi = array();
                //Ogni riga corrisponderà al numero domanda
                $r[0] = $_POST["aggiungi-risposta1"];
                $r[1] = $_POST["aggiungi-risposta2"];
                $r[2] = null;
                $r[3] = null;
                if(isset($_POST["avanzata"])){
                    $avanzata = $_POST["avanzata"];
                }else{
                    $avanzata = null;
                }
                //Controllo tematiche
                $tematica = 0;
                if($_POST["t1"] != ""){
                    $t[1] = $_POST["t1"];
                    $tematica++;
                }
                if($_POST["t2"] != ""){
                    $t[2] = $_POST["t2"];
                    $tematica++;
                }
                if($_POST["t3"] != ""){
                    $t[3] = $_POST["t3"];
                    $tematica++;
                }
                for ($i = 0; $i < 4; $i++){
                    $b[$i] = array();
                    $c[$i] = array();
                    $idb[$i] = array();
                    $idc[$i] = array();
                    $sdrisposte[$i] = array();
                    $punteggi[$i] = null;
                    $sd[$i] = null;
                }
                if($domanda == "" || $r[0] == "" || $r[1] == "" || $tematica == 0){
                    $tp["successo"] = 0;
                }else{
                    $tp["successo"] = 1;
                    foreach($_POST as $k => $v){
                        switch($k[0]){
                            case 'a':
                                if(substr($k,0,strlen($k) - 1) == "aggiungi-risposta" && $k[strlen($k) - 1] != 1 && $k[strlen($k) - 1] != 2 && $v != ""){
                                    $nr = $k[strlen($k) - 1];
                                    $r[$nr-1] = $v;
                                }else if($k == "aggiungi-approfondimento" && $v != ""){
                                    $approfondimento["testo"] = $v;
                                }else if($k == "aggiungi-approfondimento-link" && $v != ""){
                                    $approfondimento["link"] = $v;
                                }
                            break;
                            case 'r':
                                $nr = substr($k,strpos($k,"r") + 1, strpos($k, "-") - 1);
                                if(isset($r[$nr - 1]) && $r[$nr - 1] != ""){  //Controllo che la risposta sia stata scritta
                                    if(strpos($k, 'b') !== false && $v != ""){
                                        array_push($b[$nr-1], $v);      
                                    }else if(strpos($k, 'c') !== false && $v != ""){
                                        array_push($c[$nr-1],$v);
                                    }else if($v != ""){
                                        $sd[$nr-1] = $v;
                                    }
                                }
                            break;
                            case 'p':
                                if(substr($k,0,strlen($k) - 1) == "punteggio-r" && $v != ""){
                                    $nr = substr($k,strpos($k,"r") + 1);
                                    $punteggi[$nr - 1] = $v;
                                }
                            break;
                            case 's':
                                $nr = $k[strpos($k,"r") + 1];
                                array_push($sdrisposte[$nr - 1],$v);
                        }
                    }
                    //PER AGGIUNGERE TUTTO:
                    //AGGIUNGERE BENEFICIO, CONSIGLIO E APPROFONDIMENTO SINGOLARMENTE
                    foreach($b as $k => $v){
                        if(!empty($v)){
                            foreach($v as $be){
                                array_push($idb[$k],$dbh->addBenefit($be));
                            }
                        }
                    }
                    if(isset($approfondimento) && $approfondimento != null){
                        $ida = $dbh->addInfo($approfondimento["testo"],isset($approfondimento["link"]) ? $approfondimento["link"]: null)[0];
                    }else{
                        $ida = null;
                    }
                    foreach($c as $k => $v){
                        if(!empty($v)){
                            foreach($v as $co){
                                array_push($idc[$k],$dbh->addAdvice($co)[0]);
                            }
                        }
                    }
                    //AGGIUNGERE LA DOMANDA E COLLEGARLA CON LA TABELLA SVILUPPA ALLE TEMATICHE SCELTE
                    $numeroDomanda = $dbh->addQuestion($_SESSION["Idq"],$domanda,$ida,$avanzata,null) ?: 1;
                    foreach($t as $k => $peso){
                        if(isset($peso)){
                            $dbh->linkQuestionToTheme($_SESSION["Idq"], $numeroDomanda,$k, $peso);
                        }
                    }
                    //AGGIUNGERE LE RISPOSTE 
                    //COLLEGARE CON VISUALIZZA IL CONSIGLIO ALLA RISPOSTA E BENEFICIO_PER_RISPOSTA
                    for($i = 0;$i < 4;$i++){
                        $idr = $dbh->addAnswer($_SESSION["Idq"],$numeroDomanda,$i+1,isset($punteggi[$i]) ? $punteggi[$i] : null,$r[$i])[0];
                        if($idb[$i] != null){
                            foreach($idb[$i] as $idbeneficio){
                                $dbh->linkAnswerBenefit($idbeneficio,$idr);
                            }
                        }
                        if($idc[$i] != null){
                            foreach($idc[$i] as $idconsiglio){
                                $dbh->linkAnswerAdvice($idconsiglio,$idr);
                            }
                        }
                        if($sd[$i] != null){
                            $numerosottodomanda = $dbh->addQuestion($_SESSION["Idq"],$sd[$i],null,null,$idr) ?: 1;
                            $k = 1;
                            foreach($sdrisposte[$i] as $sottorisposta){
                                if(!empty($sottorisposta)){
                                    $dbh->addAnswer($_SESSION["Idq"],$numerosottodomanda,$k,isset($punteggiosd) ? $punteggiosd : null,$sottorisposta,$sottorisposta);
                                    $k++;
                                }
                            }
                        }
                    }
                }
            }else{
                $tp["successo"] = -1;
            }
        }
    }
    $tp["titolo"] = "Admin";
    $tp["nome"] = "template/admin-aggiunta-form.php";
    $tp["header"] = 3;
}
require 'template/base.php';
?>